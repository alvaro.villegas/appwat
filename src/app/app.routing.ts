import { RouterModule } from '@angular/router';
import { HomeComponent } from './features/home/home.component';
import { ContactUsComponent } from './features/contact-us/contact-us.component';
import { ServicesComponent } from './features/services/services.component';

const appRoutes = [
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  { path: 'home', component: HomeComponent},
  { path: 'contact-us', component: ContactUsComponent},
  { path: 'services', component: ServicesComponent},
];
export const routing = RouterModule.forRoot(appRoutes);
