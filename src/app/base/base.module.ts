import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from '../app.routing';
import { TopComponent } from './top/top.component';
import { FootComponent } from './foot/foot.component';
import { ContentComponent } from './content/content.component';

@NgModule({
  declarations: [TopComponent, FootComponent, ContentComponent],
  exports: [TopComponent, FootComponent, ContentComponent],
  imports: [CommonModule, routing],
})
export class BaseModule {}
